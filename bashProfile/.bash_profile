

##
# Your previous /Users/z013pcp/.bash_profile file was backed up as /Users/z013pcp/.bash_profile.macports-saved_2015-04-20_at_20:45:10
##

# MacPorts Installer addition on 2015-04-20_at_20:45:10: adding an appropriate PATH variable for use with MacPorts.
export PATH="/usr/local/Cellar/:/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

# added by Anaconda 2.2.0 installer
export PATH="/Users/z013pcp/anaconda/bin:$PATH"

# Source .bashrc at startup
source ~/.bashrc


# Set terminal colors
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

function proxyon {
  echo "Please enter Target LANID: "
  read -s LANID
  echo "Please enter Target Password: "
  read -s PASSWORD
         
  CLEAN_PASSWORD=$(echo $PASSWORD | python -c "import urllib, sys ; print urllib.quote_plus(sys.stdin.read().rstrip('\r\n'))";)
           
  export http_proxy=http://$LANID:$CLEAN_PASSWORD@proxy-mdha.target.com:8080
  export HTTP_PROXY=http://$LANID:$CLEAN_PASSWORD@proxy-mdha.target.com:8080
  export https_proxy=http://$LANID:$CLEAN_PASSWORD@proxy-mdha.target.com:8080
  export HTTPS_PROXY=http://$LANID:$CLEAN_PASSWORD@proxy-mdha.target.com:8080
}
               
function proxyoff {
  unset http_proxy
  unset HTTP_PROXY
  unset https_proxy
  unset HTTPS_PROXY
}

# added by Anaconda 2.3.0 installer
export PATH="/Users/z013pcp/anaconda/bin:$PATH"

# added by Anaconda 2.3.0 installer
export PATH="/Users/z013pcp/anaconda/bin:$PATH"
