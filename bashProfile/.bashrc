alias bigred='ssh -o StrictHostKeyChecking=no z013pcp@brdn1176.target.com'
alias bigred_edge='ssh z013pcp@redlx1005.hq.target.com'
alias subl='/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl'
alias vi='vim'
alias pip_tgt='pip install --proxy https://sqdlx0001.hq.target.com:3128'
alias p4merge='/Applications/p4merge.app/Contents/Resources/launchp4merge'
alias p4vc='/Applications/p4vc'
alias toBigred='scp -o StrictHostKeyChecking=no target/scala-2.1?/*.jar z013pcp@brdn1176.target.com:/home_dir/z013pcp/scala_repo'
alias bigredScreen='ssh z013pcp@hadlx1014.target.com && screen'
#alias svprz_bigred='ssh -o StrictHostKeyChecking=no SVPRZHDP@BIGRED.TARGET.COM'
#alias svprz_bigred='ssh -o StrictHostKeyChecking=no SVPRZHDP@hadlx1013.target.com'
alias svprz_bigred='ssh -o StrictHostKeyChecking=no SVPRZHDP@brdn1171.target.com'

source ~/.git-completion.bash
source ~/.git-prompt.sh
#PS1='[\w$(__git_ps1 " \e[0;36m(%s)\e[m")]\$ '
#PS1='[\w]$(__git_ps1 "\[\e[0;36m\]\](%s)")\[\e[m\]\$ '
CYAN=$(tput setaf 6)
PURPLE=$(tput setaf 5)
RESET=$(tput sgr0)
# \n at beginning ensures prompt always returns on new line 
PS1='\n[\w]$(__git_ps1 "\[$CYAN\](%s)")\[$RESET\]\$ '

#Note: These seem to now be required for running spark-shell locally on mac
export  SPARK_MASTER_IP=127.0.0.1
export  SPARK_LOCAL_IP=127.0.0.1

export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#export JAVA_HOME=/usr/bin/java
#export SPARK_HOME=/Users/z013pcp/software/spark/spark-1.6.1-bin-hadoop2.6
export SPARK_HOME=/Users/z013pcp/software/spark/spark-2.1.0-bin-hadoop2.7
#export SPARK_HOME=/Users/z013pcp/code/spark/spark-1.6.0-SNAPSHOT-bin-jonah_mod6
export PATH=$SPARK_HOME/bin:$PATH
#export PYTHONPATH=/Users/z013pcp/.local/lib/python2.7/site-packages
export PYTHONPATH=/Users/z013pcp/anaconda/lib/python2.7/site-packages
#export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/Users/z013pcp/anaconda/lib

#This is for rabbitmq server
export PATH=$PATH:/usr/local/sbin

export STAGE_INGEST=http://10.63.22.67 #Stage ip for personalization server 
export STAGE_SERVE=http://10.63.22.70:8080
export PROD_INGEST=http://przlx1001.hq.target.com
export PROD_SERVE=https://prz-secure.target.com

# Might need to fix/delete this
#export AWS_DEFAULT_PROFILE=MY_PROFILE

#adding this to try to get chef SSL problem to go aways
#export http_proxy=http://sqldx1002.hq.target.com

eval "$(rbenv init -)"
